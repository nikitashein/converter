import argparse
import csv
import json
import yaml

parser = argparse.ArgumentParser(description='csv to yaml or json')
parser.add_argument('file_csv', type=str, help='Name file type csv')
parser.add_argument('type_to_convert', type=str, help='type to convert')
args = parser.parse_args()


def filling_lists(titles, lines, new_list):
    item = dict(zip(titles, lines))
    new_list.append(item)


def start_script():
    open_file = open(args.file_csv, "r")
    reader_to_file = csv.reader(open_file)
    title_list = open_file.readline()[:-1].split(',')
    new_list = []
    for line in reader_to_file:
        filling_lists(title_list, line, new_list)
    if 'yaml' in args.type_to_convert:
        conv_file = open(f'{args.file_csv[:-4]}.yaml', "w")
        conv_file.write(yaml.dump(new_list, sort_keys=False, allow_unicode=True))
    elif 'json' in args.type_to_convert:
        conv_file = open(f'{args.file_csv[:-4]}.json', 'w')
        conv_file.write(json.dumps(new_list, indent=4, ensure_ascii=False))
    conv_file.close()
    open_file.close()


if __name__ == '__main__':
    start_script()
